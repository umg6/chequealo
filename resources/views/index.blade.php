@extends('app')

@section('title')
Bienvenido
@endsection

@section('content')
    <div class="banner">
        <img src="{{ URL::asset('img/banner.jpeg') }}" class="full-width img-fluid" alt="personas trabajando">
        <div class="overlay">
            <h1>El tiempo es la cosa más valiosa que una persona puede gastar. <small class="d-block text-right">- Cristiano Ronaldo</small></h1>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 login-section">
                <h2 class="mb-3 text-center">
                    Inicia sesión con tu usuario
                    <small class="d-block">o <a href="{{ route('register') }}">crea una cuenta</a></small>
                </h2>
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Correo</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                        <small id="emailHelp" class="form-text text-muted">Nunca compartiremos tu información con nadie.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Contraseña</label>
                        <input type="password" class="form-control" id="exampleInputPassword1">
                    </div>
                    <button type="submit" class="btn btn-primary">Iniciar sesión</button>
                </form>
            </div>
        </div>
    </div>
@endsection
