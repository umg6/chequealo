<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
  public function index(){
    return view('cursos.index');
  } 

  public function create(){ 
    return "";
  } 

  public function show($curso){
    return "";
  } 
}